package models

import akka.actor.{ActorRef, Props}
import play.api.libs.json._
import scala.collection.mutable.{Map => MMap}
import _root_.consts.getRandomWords

object Game {
  type Players = List[ConnectedPlayer]
  type Steps = List[GameStep]

  private var players: Players = Nil
  private var state: GameState.Value = _
  private var steps: Steps = _
  private var playerIndex = 0
  private var finishResults: List[(String, Int)] = _
  private var words: List[String] = _

  def playerConnected(out: ActorRef): Props = {
    val connectedPlayer: ConnectedPlayer = ConnectedPlayer(
      Player(s"player ${formatPlayerNumber(getNextPlayerIndex)}", isAdminPresent), out
    )
    connectedPlayer.onPlayerEvent = onPlayerEvent(connectedPlayer)

    players = connectedPlayer :: players
    updatePlayersList()
    sendConnectedMessage(connectedPlayer)
    connectedPlayer.props
  }

  def sendConnectedMessage(player: ConnectedPlayer): Unit = {
    val players = List(player)
    sendMessageForPlayers(OutMessages.connected, players, Json.obj(
      "name" -> player.name,
      "state" -> state,
      "isAdmin" -> player.isAdmin,
    ))

    if (state == GameState.running && steps.nonEmpty) {
      sendMessageForPlayers(OutMessages.nextStep, players, Json.toJson(steps.last))
      return
    }

    if (state == GameState.finished) {
      sendFinishedResults(players)
    }
  }

  def disconnected(player: ConnectedPlayer): Unit = {
    players = players.filterNot(_ == player)
    updatePlayersList()
  }

  private def updatePlayersList(): Unit = {
    val playerNames = players.map(_.name)
    sendMessageForPlayers(OutMessages.playerList, players, Json.obj("players" -> playerNames))
  }

  private def sendGameState(players: Players): Unit = {
    sendMessageForPlayers(OutMessages.gameStateChanged, players, Json.obj("state" -> state))
  }

  private def sendMessageForPlayers(messageName: OutMessages.Value, players: Players, data: JsValue): Unit = {
    players.foreach {
      _.sendMessage(messageName.toString, data)
    }
  }

  private def isAdminPresent: Boolean = !players.exists(_.isAdmin)

  private def onPlayerEvent(player: ConnectedPlayer)(eventName: PlayerEvents.Value, data: JsValue): Unit = {
    eventName match {
      case PlayerEvents.expiredTime =>
        state = GameState.finished
        sendGameState(players)
        saveFinishedResults()
        sendFinishedResults(players)
        
      case PlayerEvents.resetGame =>
        resetGame()
        sendGameState(players)

      case PlayerEvents.disconnect => disconnected(player)

      case PlayerEvents.startGame =>
        state = GameState.running
        sendGameState(players)
        nextStep()

      case PlayerEvents.playerResult =>
        if (steps.isEmpty) return

        val lastStep = steps.last
        val pos = (data \ "pos").as[Int]
        if (lastStep.pos != pos) return

        lastStep.setResult(PlayerResult((data \ "result").as[String], player))
        sendStepResults(lastStep)

        if (!lastStep.isStepFinished) return

        nextStep()
    }
  }

  private def saveFinishedResults(): Unit = {
    val resultsByPlayers = steps.flatMap(_.bestResult).groupBy(_.player.id)
    finishResults = players.map {
      p => p.name -> resultsByPlayers.get(p.id).map(_.length).getOrElse(0)
    }

    println(Json.prettyPrint(Json.obj("results" -> finishResults)))
  }

  private def sendFinishedResults(sendForPlayers: Players): Unit = {
    sendMessageForPlayers(OutMessages.finishResults, sendForPlayers, Json.obj("results" -> finishResults))
  }

  private def sendStepResults(step: GameStep): Unit = {
    val results = step.playerResults.toList.map { case (_, result) => Json.obj(
      "player" -> result.player.name,
      "result" -> result.result)
    }
    sendMessageForPlayers(OutMessages.stepResults, players, Json.obj("results" -> results))
  }

  private def resetGame(): Unit = {
    state = GameState.ready
    steps = List()
    finishResults = null
    words = getRandomWords
  }

  implicit val stepWrites: Writes[GameStep] = (step: GameStep) => Json.obj(
    "task" -> step.task,
    "pos" -> step.pos,
  )

  private def nextStep(): Unit = {
    val nextStep = GameStep(words(steps.size % words.size), steps.size)
    steps = steps ++ List(nextStep)
    sendMessageForPlayers(OutMessages.nextStep, players, Json.toJson(nextStep))
  }

  private def getNextPlayerIndex = {
    playerIndex += 1
    playerIndex
  }

  private def formatPlayerNumber(n: Int) = f"$n%04d"

  resetGame()
}

case class PlayerResult(result: String, player: ConnectedPlayer)

case class GameStep(task: String, pos: Int) {
  val playerResults: MMap[Int, PlayerResult] = MMap()

  def setResult(result: PlayerResult): Unit = {
    playerResults(result.player.id) = result
  }

  def isStepFinished: Boolean = findBestResult.nonEmpty

  def bestResult: Option[PlayerResult] = findBestResult
    .map(_._2)

  private def findBestResult: Option[(Int, PlayerResult)] = playerResults.toList
    .find { case (_, result) => result.result.length == task.length }
}

private object OutMessages extends Enumeration {
  val connected: Value = Value("connected")
  val playerList: Value = Value("playerList")
  val gameStateChanged: Value = Value("gameStateChanged")
  val nextStep: Value = Value("nextStep")
  val stepResults: Value = Value("stepResults")
  val finishResults: Value = Value("finishResults")
}

private object GameState extends Enumeration {
  val ready: Value = Value("ready")
  val running: Value = Value("running")
  val finished: Value = Value("finished")
}
