marshal = JSON.stringify.bind JSON
unMarshal = JSON.parse.bind JSON

ws = new WebSocket appConf.socketUrl

sendMessage = (message, data) -> ws.send marshal {message, data}

initTime = 60

gameStates =
    onStart: "ready"
    isRunning: "running"
    isFinished: "finished"

messages =
    run: "run"
    reset: "reset"
    playerTry: "playerTry"
    heartBeat: "heartBeat"
    expiredTime: "expiredTime"

app = new Vue
    el: "#app"

    data:
        state : ""
        playerName : ""
        players : []
        isAdmin : no
        step : {}
        usersResult : []
        finishResults : []
        currentTime : 0
        startTime : 0
        timerThread : null
        isTimerRun : false
        needCountdown : true
        isUpTime : false

    methods:
        doGame: ->
            console.log("doGame")
            console.log(@state)
            if @state is gameStates.onStart
                console.log("before onRun")
                @onRun()
            else if @state is gameStates.isFinished
                console.log("before resetGame")
                @resetGame()
        gameOnStart: -> @state is gameStates.onStart
        gameIsRunning: -> @state is gameStates.isRunning
        gameIsFinished: -> @state is gameStates.isFinished
        expiredTime: -> 
            console.log("expiredTime")
            sendMessage messages.expiredTime, null
        onRun: -> 
            console.log("onRun")
            sendMessage messages.run, null
        resetGame: -> 
            console.log("resetGame")
            if !@isTimerRun and !@needCountdown
                sendMessage messages.reset, null
        onSuccessTry: (pos, subtask) -> 
            console.log("onSuccessTry - ", pos, " ", subtask)
            sendMessage messages.playerTry, {pos, result: subtask}
        
        getResetButtonCssClass: -> {invisible: @state is gameStates.isRunning}
        
        resetCountdown: ->
            @needCountdown = false

        runTimer: ->
            if @currentTime > 0 
                @currentTime = initTime - (new Date().getTime() - @startTime) / 1000
            else
                @currentTime = 0
                do @expiredTime

        prepareTimer: ->
            @startTime = new Date().getTime()
            @currentTime = initTime 
            @isTimerRun = true
            @timerThread = setInterval(@runTimer, 10)
            setTimeout(@resetCountdown, 1000)
        
        hideTimeUp: ->
            @isUpTime = false

messageCallbacks =
    playerList: ({players}) ->
        app.players = players

    gameStateChanged: ({state}) ->
        console.log(state)
        if state is "running" 
            app.needCountdown = true
            setTimeout(app.prepareTimer, 2000)
        if state is "finished" 
            app.isTimerRun = false
            clearInterval(app.timerThread)

        app.state = state

    connected: ({name, state, isAdmin}) ->
        app.playerName = name
        app.state = state
        app.isAdmin = isAdmin

    nextStep: (step) ->
        console.log("nextStep ", step)
        app.step = step
        app.usersResult = []
        app.startTime += 5000
        if app.isTimerRun
            app.isUpTime = true
            setTimeout(app.hideTimeUp, 1000)

    heartBeat: ->
    stepResults: ({results}) -> app.usersResult = results
    finishResults: ({results}) ->
        console.log("finishResults", results)
        app.finishResults = results
            .sort ([__, r1], [_1, r2]) -> r2 - r1
            .map ([name, wins]) -> {name, wins}

ws.onmessage = ({data}) ->
    unless data then throw new Error "empty data"
    obj = unMarshal data
    messageCallbacks[obj.message] obj.data

ws.onopen = ->
    while yes
        await window.sleep 15000
        sendMessage messages.heartBeat, Date.now()
