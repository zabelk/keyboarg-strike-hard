window.sleep = (time) -> new Promise (res) -> setTimeout res, time

Vue.component "start-game", {
    template: """
        <div class="start-game">
            <div class="useless-keys">
                <span class="useless">Space</span>
                <span class="useless">Backspace</span>
                <span class="useless">Enter</span>
            </div>
            <h1>are you ready for<br/> <span class="game-name">keyboarg strike</span><br/>???!!!</h1>
            <ul class="user-list">
                <li v-for="player in players">{{player}}</li>
            </ul>
        </div>
    """
    props: ["players"]
}

Vue.component "game-results", {
    template: """
        <div class="game-results">
            <ul>
                <li v-for="result in results">
                    {{result.name}} -> {{result.wins}}
                </li>
            </ul>
        </div>
    """
    props: ["results"]
}

initCountdown = 2

Vue.component "run-game", {
    template: """
        <div class="run-game">
            <div class="task-container" v-if="isStepActive()">
                <div class="task-word">
                    <ul class="task">
                        <li v-bind:class="getLetterCssClass(index)" v-for="(letter, index) in letters">{{letter}}</li>
                    </ul>
                </div>

                <div class="users-results">
                    <ul v-if="results.length">
                        <li v-for="result in results">{{result.player}}: {{result.result}}</li>
                    </ul>
                </div>
            </div>

            <div class="get-ready-block" v-if="!isStepActive()">
                <span class="ready-message">ready {{countdown}} !!!</span>
            </div>
        </div>
    """
    props: ["step", "onSuccessTry", "results", "needCountdown"]
    watch:
        step: ->
            @currentLetter = null
            @errorLetter = null
            do @initNextStep

    data: ->
        keyEvent: "keydown"
        letters: []
        currentLetter: null
        errorLetter: null
        countdown: null

        initNextStep: ->
            await @showPrepareMessage()
            @letters = @step.task.split ''
            @currentLetter = 0
            @errorLetter = null

        keyListener: (e) =>
            return if @currentLetter is null
            key = getKeySymbol e.code
            @playerTry key

        playerTry: (key) ->
            @errorLetter = null
            return do @successTry if key is @letters[@currentLetter]
            @errorLetter = @currentLetter

        successTry: ->
            @currentLetter++
            @onSuccessTry @step.pos, @step.task.slice 0, @currentLetter

        showPrepareMessage: ->
            @countdown = initCountdown
            if @needCountdown
                while @countdown
                    await sleep 1000
                    @countdown--

    methods:
        getLetterCssClass: (index) ->
            "letter-done": index < @currentLetter
            "has-error": index is @errorLetter

        isStepActive: -> @currentLetter isnt null

    mounted: -> document.addEventListener @keyEvent, @keyListener
    beforeDestroy: -> document.removeEventListener @keyEvent, @keyListener
}

specialCodes = {
    Space: " "
}

remap = {
    'A' : 'B',
    'B' : 'A',
    
    'T' : 'K',
    'K' : 'T',

    'E' : 'N',
    'N' : 'O',
    'O' : 'E',

    'C' : 'Y',
    'Y' : 'M',
    'M' : 'P',
    'P' : 'C',
    
    'L' : 'W',
    'W' : 'L'
}

getKeySymbol = (keyboardCode) ->
    return specialCodes[keyboardCode] if specialCodes[keyboardCode]

    possibleLetter = keyboardCode.replace /^(Key|Digit)([\w])$/ig, "$2"
    possibleLetter = remap[possibleLetter] if remap[possibleLetter]
    console.log possibleLetter
    return null if possibleLetter is keyboardCode

    possibleLetter.toLowerCase()
